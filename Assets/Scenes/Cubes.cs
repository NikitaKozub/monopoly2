﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Кубики для действий
/// </summary>
public class Cubes : MonoBehaviour
{
    public static System.Random rng;

    /// <summary>
    /// Значение 1 кубика
    /// </summary>
    public static int Value1 { set; get; }

    /// <summary>
    /// Значение 2 кубика
    /// </summary>
    public static int Value2 { set; get; }

    /// <summary>
    /// Общий счет
    /// </summary>
    public static int Result { set; get; }

    static Cubes()
    {
        rng = new System.Random();
    }

    /// <summary>
    /// Вычисляет случайное значение от 2 до 12
    /// </summary>
    public static void ThrowCubes()
    {
        Value1 = rng.Next(1, 6);
        Value2 = rng.Next(1, 6);
        Result = Value1 + Value2;
    }
}
