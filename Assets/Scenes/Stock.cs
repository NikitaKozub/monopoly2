﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Рынок акций
/// </summary>
public class Stock : MonoBehaviour
{
    int[] Stocks;
    public Dictionary<int, string> stockNumberColor = new Dictionary<int, string>();
    public Stock()
    {
        Stocks = new int[8];
        StockStart();
        stockNumberColor.Add(0, "Brown");
        stockNumberColor.Add(1, "Green");
        stockNumberColor.Add(2, "Violet");
        stockNumberColor.Add(3, "LightBlue");
        stockNumberColor.Add(4, "Yellow");
        stockNumberColor.Add(5, "Red");
        stockNumberColor.Add(6, "Blue");
        stockNumberColor.Add(7, "Pink");
    }

    /// <summary>
    /// Начать рыночную торговлю
    /// </summary>
    public void StockStart()
    {
        for (int i = 0; i < Stocks.Length; i++)
        {
            Stocks[i] = 0;
        }
    }

    /// <summary>
    /// Повысить цены акций
    /// </summary>
    /// <param name="idCompany">цветовой номер компанииот 1 до 8</param>
    /// <param name="number"> на сколько пунктов поднять</param>
    public void IncreaseStockValue(int idCompany, int number)
    {
        Stocks[idCompany] += number;
        if (Stocks[idCompany] > 5)
        {
            Stocks[idCompany] = 5;
        }
    }

    /// <summary>
    /// Понизить цены акций
    /// </summary>
    /// <param name="idCompany">цветовой номер компанииот 1 до 8</param>
    /// <param name="number">на сколько нужно опустить</param>
    public void ReduceStockValue(int idCompany, int number)
    {
        Stocks[idCompany] -= number;
        if (Stocks[idCompany] < -5)
        {
            Stocks[idCompany] = -5;
        }
    }

    /// <summary>
    /// Получить цену акций компании по цветовому обохначению
    /// </summary>
    /// <param name="idCompany">цветовой номер компанииот 1 до 8</param>
    /// <returns>возвращает стоимость</returns>
    public int GetStockValue(int idCompany)
    {
        return Stocks[idCompany];
    }
}
