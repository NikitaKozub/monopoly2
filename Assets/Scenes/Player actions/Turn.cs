﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.UI;
using System;

public class Turn : MonoBehaviour
{
    public Board Board;
    public Event currentEvent;
    public int numberEvent;

    public Turn(Board board)
    {
        Board = board;
        numberEvent = 0;
    }

    /// <summary>
    /// Передвижение персонажа
    /// </summary>
    public void MovePlayer(int step, int CurrentPlayer)
    {
        Board.Player[CurrentPlayer].Move(step);
        if (Board.Cells[Board.Player[CurrentPlayer].Position].IsEvent == true)
        {
            currentEvent = Board.Events[numberEvent];
            Board.Player[CurrentPlayer].ApplyEvent(currentEvent);
            numberEvent++;
        }
        if (numberEvent == 20)
        {
            numberEvent = 0;
        }
    }

    /// <summary>
    /// Купить компанию
    /// </summary>
    public bool BuyCompany(int CurrentPlayer)
    {
        if (Board.Cells[Board.Player[CurrentPlayer].Position].Company_ != null 
            && Board.Cells[Board.Player[CurrentPlayer].Position].Company_.Owner == null)
        {
            Board.Player[CurrentPlayer].BuyCompany(Board.Cells[Board.Player[CurrentPlayer].Position].Company_);
            return true;
        }
        return false;
    }
}
