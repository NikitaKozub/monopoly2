﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Доска
/// </summary>
public class Board : MonoBehaviour
{
    /// <summary>
    /// Количество клеток на доске
    /// </summary>
    public const int SIZE_BOARD = 40;
    public Cell[] Cells;
    public Player[] Player;
    public Event Event;
    public Stock StockS;
    public IList<Event> Events;

    public Board(Player[] players)
    {
        Cells = new Cell[SIZE_BOARD];
        Player = players;
        Event = new Event();
        StockS = new Stock();
    }

    /// <summary>
    /// Создать доску с клетками
    /// </summary>
    public void CreateBoard()
    {
        Company company = new Company();
        for (int i = 0; i < Cells.Length; i++)
        {
            Cells[i] = new Cell();
            Cells[i].Id = i;
            Cells[i].IsEvent = false;
            if (i == 0)
            {
                continue;
            }
            if (i == 10)
            {
                continue;
            }
            if (i == 20)
            {
                continue;
            }
            if (i == 3)
            {
                Cells[i].Id = i;
                Cells[i].IsEvent = true;
                continue;
            }
            if (i == 4)
            {
                Cells[i].Id = i;
                Cells[i].IsEvent = true;
                continue;
            }
            if (i == 8)
            {
                Cells[i].Id = i;
                Cells[i].IsEvent = true;
                continue;
            }
            if (i == 12)
            {
                Cells[i].Id = i;
                Cells[i].IsEvent = true;
                continue;
            }
            if (i == 16)
            {
                Cells[i].Id = i;
                Cells[i].IsEvent = true;
                continue;
            }
            if (i == 22)
            {
                Cells[i].Id = i;
                Cells[i].IsEvent = true;
                continue;
            }
            if (i == 28)
            {
                Cells[i].Id = i;
                Cells[i].IsEvent = true;
                continue;
            }
            if (i == 32)
            {
                Cells[i].Id = i;
                Cells[i].IsEvent = true;
                continue;
            }
            if (i == 37)
            {
                Cells[i].Id = i;
                Cells[i].IsEvent = true;
                continue;
            }
            if (i == 39)
            {
                Cells[i].Id = i;
                Cells[i].IsEvent = true;
                continue;
            }
            Cells[i].Company_ = company.CreateCompany(i, "Red", "Роскосмос", 400, 10, 20);
        }
        Events = Event.CreateDeck();
    }
}
