﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Клетка доски
/// </summary>
public class Cell
{
    /// <summary>
    /// Номер клетки
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// Есть ли там событие
    /// </summary>
    public bool IsEvent { get; set; }
    /// <summary>
    /// Есть ли там комапния
    /// </summary>
    public Company Company_ { get; set; }
}
