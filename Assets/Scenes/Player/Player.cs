﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    /// <summary>
    /// Имя игрока
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Список компаний принадлежащих гроку
    /// </summary>
    public List<Company> Companies { get; set; }
    /// <summary>
    /// Деньги на счету
    /// </summary>
    public int Sum { get; set; }
    /// <summary>
    /// Позиция
    /// </summary>
    public int Position { get; set; }
    /// <summary>
    /// Количество ходов в тюрьме
    /// </summary>
    public int StepPrison { get; set; }
    /// <summary>
    /// Цвет на доске
    /// </summary>
    public Color Color { get; set; }
    /// <summary>
    /// Сколько компаний одного цвета "Red", 3
    /// </summary>
    public Dictionary<string, int> ColorCompany;
    public Player(string name)
    {
        Name = name;
        Companies = new List<Company>();
        Sum = 1500;
        Position = 0;
        StepPrison = 0;
        Color = Color.red;
        ColorCompany = new Dictionary<string, int>();
        ColorCompany.Add("Red", 3);
        ColorCompany.Add("Blue", 3);
        ColorCompany.Add("Brown", 2);
        ColorCompany.Add("LightBlue", 3);
        ColorCompany.Add("Pink", 2);
        ColorCompany.Add("Violet", 3);
        ColorCompany.Add("Yellow", 3);
        ColorCompany.Add("Green", 3);
    }

    /// <summary>
    /// Увеличить деньги игрока
    /// </summary>
    /// <param name="sum">полученные деньги</param>
    public void IncreaseAmount(int sum)
    {
        Sum += sum;
    }

    /// <summary>
    /// Уменьшить деньги игрока
    /// </summary>
    /// <param name="sum">полученные деньги</param>
    public void ReduceAmount(int sum)
    {
        Sum -= sum;
    }

    /// <summary>
    /// Перемещение игрока
    /// </summary>
    /// <param name="step"> на сколько переместиться</param>
    public void Move(int step)
    {
        IsPrison();
        if (StepPrison == 0 || StepPrison == 3)
        {
            StepPrison = 0;
            Position += step;
            if (Position > 39)
            {
                Position = Position - 39;
                Sum +=  200;//за проход по клетке старт
            }
            if (Position < 0)
            {
                Position = Position + 39;
            }
            if (Position == 10)//тюрьма
            {
                StepPrison++;
            }
        }
        else
        {
            StepPrison++;
        }
    }

    /// <summary>
    /// Проверка на выход их тюрьмы
    /// </summary>
    public void IsPrison()
    {
        if (Cubes.Value1 == Cubes.Value2)
        {
            StepPrison = 0;
        }
    }

    /// <summary>
    /// Применить событие
    /// </summary>
    /// <param name="event_"></param>
    public void ApplyEvent(Event event_)
    {
        if (event_.Sum != 0)
        {
            Sum += event_.Sum;
        }
        if (event_.Move != 0)
        {
            Move(event_.Move);
        }
    }

    /// <summary>
    /// Оплата выхода из тюрьмы
    /// </summary>
    public void PayRansom()
    {
        Sum -= 150;
        StepPrison = 0;
    }

    /// <summary>
    /// Купить команию
    /// </summary>
    /// <param name="company"></param>
    public void BuyCompany(Company company)
    {
        company.Owner = this;
        Sum -= company.Price;
        FillInColorCard(company);
    }

    /// <summary>
    /// Пометить компанию определенного цвета, как купленную
    /// </summary>
    /// <param name="company"></param>
    private void FillInColorCard(Company company)
    {
        ColorCompany[company.Color]++;
    }

    /// <summary>
    /// Оплата за посещение чужой компании
    /// </summary>
    /// <param name="company">компания</param>
    public void PayTax(Company company)
    {
        if (company.Owner != this)
        {
            if (company.Owner.ColorCompany[company.Color] == 0)
            {
                this.Sum -= company.FullTax;
                company.Owner.Sum += company.FullTax;
            }
            this.Sum -= company.Tax;
            company.Owner.Sum += company.Tax;
        }
    }
}