﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public Player[] Players { get; set; }

    public Score(Player[] players)
    {
        Players = players;
    }
}
