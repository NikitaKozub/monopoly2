﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

public class Game : MonoBehaviour
{
    public Button ButtonTurn;
    public Button ButtonBuyCompany;
    public Button ButtonStock;

    public Board Board;
    public Turn Turn;
    private Score Score;
    private GameUI GameUI;
    public int CurrentPlayer;

    public delegate void BuyCompanyUI(int position, Color color);
    public event BuyCompanyUI buyCompanyUI;
    public delegate void UpdateUiScore(string name, int money);
    public event UpdateUiScore updateUiScore;
    public delegate void UpdateUiCubes();
    public event UpdateUiCubes updateUiCubes;
    public delegate void UpdateUiPosition(int position);
    public event UpdateUiPosition updateUiPosition;

    // Start is called before the first frame update
    void Start()
    {
        Player[] players = new Player[1] { new Player("Nikita") };
        Board = new Board(players);
        Board.CreateBoard();
        Turn = new Turn(Board);
        Score = new Score(players);
        CurrentPlayer = 0;
        GameUI = new GameUI();

        buyCompanyUI += GameUI.BuyCompanyUI;
        updateUiScore += GameUI.UpdateUiScore;
        updateUiCubes += GameUI.UpdateUiCubes;
        updateUiPosition += GameUI.UpdateUiPosition;

        ButtonTurn = GameObject.Find("Turn").GetComponent<Button>();
        ButtonTurn.onClick.AddListener(GameMove);
        ButtonBuyCompany = GameObject.Find("BuyCompany").GetComponent<Button>();
        ButtonBuyCompany.onClick.AddListener(GameBuyCompany);
        ButtonStock = GameObject.Find("StockButton").GetComponent<Button>();
        ButtonStock.onClick.AddListener(GameStock);
    }

    // Update is called once per frame
    void Update()
    { 
    }

    /// <summary>
    /// Передвижение персонажа
    /// </summary>
    public void GameMove()
    { 
        Cubes.ThrowCubes();
        int prevPosition = Board.Player[CurrentPlayer].Position;
        updateUiCubes();
        Turn.MovePlayer(Cubes.Result, CurrentPlayer);
        int nextPosition = Board.Player[CurrentPlayer].Position;
        updateUiPosition(Board.Player[CurrentPlayer].Position);
        
        if (prevPosition != nextPosition)
        {
            GameUI.MovePlayerUI();
        }
        updateUiScore(Score.Players[CurrentPlayer].Name, Score.Players[CurrentPlayer].Sum);
    }

    
    public void GameBuyCompany()
    {
        if (Turn.BuyCompany(CurrentPlayer))
        {
            buyCompanyUI(Board.Player[CurrentPlayer].Position, Board.Player[CurrentPlayer].Color);
            updateUiScore(Score.Players[CurrentPlayer].Name, Score.Players[CurrentPlayer].Sum);
        }
    }

    public void GameStock()
    {
        GameUI.StockManagerActive();
        //Board.StockS.IncreaseStockValue(0,1);
        //GameStockUI(0, "+");
    }
    public void GameDownStock()
    {
        Board.StockS.ReduceStockValue(0, 1);
        GameUI.GameStockUI(Board.StockS , 0, "-");
    }
    
    /// <summary>
    /// При смене хода Смена игрока
    /// </summary>
    public void EndTurnPlayer()
    {
        CurrentPlayer++;
        if (CurrentPlayer > Board.Player.Length)
        {
            CurrentPlayer = 0;
        }
    }
}
