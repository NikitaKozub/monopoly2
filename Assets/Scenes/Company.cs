﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Класс компания
/// </summary>
public class Company
{
    /// <summary>
    /// Id компании
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Номер клетки где находится компания
    /// </summary>
    public int IdCell { get; set; }

    /// <summary>
    /// Название
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Цвет
    /// </summary>
    public string Color { get; set; }

    /// <summary>
    /// Цена компании
    /// </summary>
    public int Price { get; set; }

    /// <summary>
    /// Цена компании
    /// </summary>
    public int Tax { get; set; }

    /// <summary>
    /// Цена компании
    /// </summary>
    public int FullTax { get; set; }

    /// <summary>
    /// Принадлжность
    /// </summary>
    public Player Owner { get; set; }

    /// <summary>
    /// Количество домов
    /// </summary>
    public int CountHouse { get; set; }

    /// <summary>
    /// Количество отелей
    /// </summary>
    public int CountHotel { get; set; }

    /// <summary>
    /// Добавить дом
    /// </summary>
    public void AddHouse()
    {
        if (CountHouse < 4 && CountHotel == 0)
        {
            CountHouse++;
        }
    }

    /// <summary>
    /// Добавить отель
    /// </summary>
    public void AddHotel()
    {
        if (CountHouse == 4)
        {
            CountHotel = 1;
            CountHouse = 0;
        }
    }
    
    /// <summary>
    /// Добавить компанию
    /// </summary>
    /// <param name="id">Номер</param>
    /// <param name="color">Цвет</param>
    /// <param name="name">Название</param>
    /// <param name="price">Цена покупки</param>
    /// <param name="tax">Плата за нахождение</param>
    /// <param name="fulltax">Плата за нахождение, если есть монополия</param>
    /// <returns></returns>
    public Company CreateCompany(int id, string color, string name, int price, int tax, int fulltax)
    {
        Company company = new Company();
        company.Id = id;
        company.Color = color;
        company.CountHotel = 0;
        company.CountHouse = 0;
        company.Name = name;
        company.Price = price;
        company.Owner = null;
        company.Tax = tax;
        company.FullTax = fulltax;
        return company;
    }
}
