﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
    public GameObject Player;
    public GameObject Title;
    public GameObject StockValue;
    public GameObject StockManager;

    

    private UnityEngine.UI.Text ScoreText;
    private UnityEngine.UI.Text CubesText;
    private UnityEngine.UI.Text PositionText;


    int moveRight = 10;
    int moveDown = 0;
    int moveLeft = 0;
    int moveUp = 0;

    public GameUI()
    {
        Player = GameObject.Find("Player1");
        ScoreText = GameObject.Find("PlayerMoney").GetComponent<Text>();
        CubesText = GameObject.Find("Cubes").GetComponent<Text>();
        PositionText = GameObject.Find("Position").GetComponent<Text>();

        StockManager = GameObject.Find("StockManager");

        //ScoreText.text = Score.Players[CurrentPlayer].Name + "Деньги: " + Score.Players[CurrentPlayer].Sum.ToString();
        CubesText.text = "Бросок: 0";
        PositionText.text = "Позиция: 0";

        StockManagerInactive();
    }

    public void StockManagerInactive()
    {
        StockManager.SetActive(false);
    }

    public void StockManagerActive()
    {
        StockManager.SetActive(true);
    }

    /// <summary>
    /// Передвижение персонажа на экране
    /// </summary>
    public void MovePlayerUI()
    {
        for (int i = 0; i < Cubes.Result; i++)
        {
            //right
            if (moveRight > 0)
            {
                Player.transform.Translate(new Vector3(10, 0, 0));
                moveRight--;
                if (moveRight == 0)
                {
                    moveDown = 10;
                }
                continue;
            }
            //down
            if (moveDown > 0)
            {
                Player.transform.position += new Vector3(0, -10, 0);
                moveDown--;
                if (moveDown == 0)
                {
                    moveLeft = 10;
                }
                continue;
            }
            //left
            if (moveLeft > 0)
            {
                Player.transform.Translate(new Vector3(-10, 0, 0));
                moveLeft--;
                if (moveLeft == 0)
                {
                    moveUp = 10;
                }
                continue;
            }
            //up
            if (moveUp > 0)
            {
                Player.transform.Translate(new Vector3(0, 10, 0));
                moveUp--;
                if (moveUp == 0)
                {
                    moveRight = 10;
                }
                continue;
            }
        }
    }

    /// <summary>
    /// Визуально отобразить покупку компании
    /// </summary>
    public void BuyCompanyUI(int position, Color color)
    {
        Title = GameObject.Find("Title" + position);
        Title.GetComponent<Renderer>().material.color = color;
    }

    public void UpdateUiScore(string name, int money)
    {
        ScoreText.text = name + " Счет: " + money.ToString();
    }

    public void UpdateUiCubes()
    {
        CubesText.text = "Бросок: " + Cubes.Result.ToString();
    }
    public void UpdateUiPosition(int position)
    {
        PositionText.text = "Позиция: " + position.ToString();
    }

    public void GameStockUI(Stock StockS, int number, string znakUpOrDown)
    {
        StockValue = GameObject.Find(StockS.stockNumberColor[number] + StockS.GetStockValue(number));
        StockValue.GetComponent<Renderer>().material.color = Color.cyan;
        if (znakUpOrDown == "+")
        {
            if (StockS.GetStockValue(number) <= 0)
            {
                StockValue = GameObject.Find(StockS.stockNumberColor[number] + (StockS.GetStockValue(number) - 1));
                StockValue.GetComponent<Renderer>().material.color = Color.grey;
            }
        }
        if (znakUpOrDown == "-")
        {
            if (StockS.GetStockValue(number) >= 0)
            {
                StockValue = GameObject.Find(StockS.stockNumberColor[number] + (StockS.GetStockValue(number) + 1));
                StockValue.GetComponent<Renderer>().material.color = Color.grey;
            }
        }
    }
}
