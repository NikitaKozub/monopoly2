﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Event : MonoBehaviour
{
    /// <summary>
    /// Количество событий в колоде
    /// </summary>
    public const int COUNT_EVENTS = 20;
    /// <summary>
    /// Номер события
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// Описание события
    /// </summary>
    public string Descritpion { get; set; }
    /// <summary>
    /// Сумма которую можно получить или потерять
    /// </summary>
    public int Sum { get; set; }
    /// <summary>
    /// На сколько поэно переместиться(+ вперед, - назад)
    /// </summary>
    public int Move { get; set; }

    /// <summary>
    /// Лист с событиями
    /// </summary>
    List<Event> Events;
    public System.Random rng;

    public Event()
    {
        Events = new List<Event>(COUNT_EVENTS);
        rng = new System.Random();
    }

    /// <summary>
    /// Чтение событий
    /// </summary>
    public void ReadEvents()
    {

    }

    /// <summary>
    /// Перемешивание событий
    /// </summary>
    public void EventMixing()
    {
        for (int i = Events.Count - 1; i >= 1; i--)
        {
            int j = rng.Next(i + 1);
            // обменять значения data[j] и data[i]
            var temp = Events[j];
            Events[j] = Events[i];
            Events[i] = temp;
        }
    }

    public void DataPreparation()
    {
        ReadEvents();
        EventMixing();
    }

    /// <summary>
    /// Создать колоду с событиями
    /// </summary>
    /// <returns></returns>
    public IList<Event> CreateDeck()
    {
        Events.Add(new Event() { Id = 0, Sum = -200, Descritpion = "Заплатите " + Sum, Move = 0 });
        Events.Add(new Event() { Id = 1, Sum = 200, Descritpion = "Получите " + Sum, Move = 0 });
        Events.Add(new Event() { Id = 2, Sum = 200, Descritpion = "Получите " + Sum, Move = 0 });
        Events.Add(new Event() { Id = 3, Sum = 50, Descritpion = "Получите " + Sum, Move = 0 });
        Events.Add(new Event() { Id = 4, Sum = 0, Move = 1, Descritpion = "Передвинтесь на " + Move });
        Events.Add(new Event() { Id = 5, Sum = 0, Move = 2, Descritpion = "Передвинтесь на " + Move });
        Events.Add(new Event() { Id = 6, Sum = 0, Move = -10, Descritpion = "Передвинтесь на " + Move });
        Events.Add(new Event() { Id = 7, Sum = -40, Move = -10, Descritpion = "Передвинтесь на " + Move + "и получите " + Sum });
        Events.Add(new Event() { Id = 8, Sum = 60, Move = 10, Descritpion = "Передвинтесь на " + Move + "и получите " + Sum });
        Events.Add(new Event() { Id = 9, Sum = -110, Move = -10, Descritpion = "Передвинтесь на " + Move + "и получите " + Sum });
        Events.Add(new Event() { Id = 10, Sum = 50, Move = 20, Descritpion = "Передвинтесь на " + Move + "и получите " + Sum });
        Events.Add(new Event() { Id = 11, Sum = -200, Descritpion = "Заплатите " + Sum, Move = 0 });
        Events.Add(new Event() { Id = 12, Sum = 200, Descritpion = "Получите " + Sum, Move = 0 });
        Events.Add(new Event() { Id = 13, Sum = 200, Descritpion = "Получите " + Sum, Move = 0 });
        Events.Add(new Event() { Id = 14, Sum = 50, Descritpion = "Получите " + Sum, Move = 0 });
        Events.Add(new Event() { Id = 15, Sum = 0, Move = 1, Descritpion = "Передвинтесь на " + Move });
        Events.Add(new Event() { Id = 16, Sum = 0, Move = 2, Descritpion = "Передвинтесь на " + Move });
        Events.Add(new Event() { Id = 17, Sum = 0, Move = -10, Descritpion = "Передвинтесь на " + Move });
        Events.Add(new Event() { Id = 18, Sum = -40, Move = -10, Descritpion = "Передвинтесь на " + Move + "и получите " + Sum });
        Events.Add(new Event() { Id = 19, Sum = 60, Move = 10, Descritpion = "Передвинтесь на " + Move + "и получите " + Sum });
        Events.Add(new Event() { Id = 20, Sum = -110, Move = -10, Descritpion = "Передвинтесь на " + Move + "и получите " + Sum });
        EventMixing();
        return Events;
    }
}


